#![feature(integer_atomics)]
#![feature(box_syntax)]

#[macro_use]
extern crate lazy_static;

pub mod basic_codes;
pub mod atomic_f64;
