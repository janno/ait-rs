
//// STIRLING APPROXIMATIONS
pub mod stirling_approximation{
    use std::f64;
    #[inline]
    pub fn fac_stirling(n: f64) -> f64{
        let result: f64 = (2.0*f64::consts::PI*n).sqrt();
        let frac: f64 = (n/f64::consts::E).powf(n);
        result * frac / f64::consts::LN_2
    }


    use std;
    use atomic_f64::AtomicF64;

    const MAX_LOG_FAC_MEMOIZATION: usize = 1024*1024;

    lazy_static!{
        static ref LOG_FAC : Box<[AtomicF64; MAX_LOG_FAC_MEMOIZATION]> = {
            let mut result : Box<[AtomicF64; MAX_LOG_FAC_MEMOIZATION]> = unsafe { box std::mem::uninitialized() };
            for i in 0..MAX_LOG_FAC_MEMOIZATION {
                let uninit = std::mem::replace(&mut result[i], AtomicF64::new(0.0));
                std::mem::forget(uninit)
            };
            result
        };
    }

    #[inline]
    pub fn log_fac_stirling(n: usize) -> f64{
        let mut result : f64;
        if n < MAX_LOG_FAC_MEMOIZATION {
            let result = LOG_FAC[n-1].get();
            if result != 0.0 {
                return result
            }
        }
        result = ((n*n) as f64).ln() - (n as f64);
        result /= f64::consts::LN_2;
        if n < MAX_LOG_FAC_MEMOIZATION {
            LOG_FAC[n-1].set(result)
        }
        result
    }


    #[inline]
    pub fn log_binomial_coefficient_stirling(n: usize, k: usize) -> f64{
        let res: f64;
        if n == k{
            res = 0.0;
        } else {
            if (n == 0) || (k == 0) {
                res = f64::INFINITY;
            } else {
                // let n = n as f64;
                // let k = k as f64;
                res = log_fac_stirling(n) - log_fac_stirling(k) - log_fac_stirling(n - k);
            }
        }

        res
    }


    #[inline]
    pub fn gammaln_stirling(z: f64) -> f64{
        let result: f64 = z*z.ln() - z + 0.5*(2.0*f64::consts::PI/z).ln();
        result / f64::consts::LN_2
    }
}

//// DATA-TO-MODEL FOR DISTRIBUTIONS
pub mod d2m_codes{
    use basic_codes::stirling_approximation;

    #[inline]
    pub fn d2m_nonzero_distribution_cost(counts: usize, bins: usize) -> f64{
        //println!("approximating d2m NONZERO distribution cost: {} counts in {} bins", counts, bins);
        if counts < bins{
            panic!("Number of counts in non-zero distribution must be >= number of bins!");
        }
        else if bins == 1
        {
            return 0.0;
        }
        stirling_approximation::log_binomial_coefficient_stirling(counts - 1, bins - 1)
    }

    #[inline]
    pub fn d2m_distribution_cost(counts: usize, bins: usize) -> f64{
        //println!("approximating d2m distribution cost: {} counts in {} bins", counts, bins);
        if bins == 1
        {
            return 0.0;
        }
        stirling_approximation::log_binomial_coefficient_stirling(counts + bins - 1, bins - 1)

    }
}


//// uic

pub mod uic{
    use std;
    use atomic_f64::AtomicF64;

    const MAX_UIC_MEMOIZATION: usize = 1024*1024;

    lazy_static!{
        static ref UIC : Box<[AtomicF64; MAX_UIC_MEMOIZATION]> = {
            let mut result : Box<[AtomicF64; MAX_UIC_MEMOIZATION]> = unsafe { box std::mem::uninitialized() };
            for i in 0..MAX_UIC_MEMOIZATION {
                let uninit = std::mem::replace(&mut result[i], AtomicF64::new(0.0));
                std::mem::forget(uninit)
            };
            result
        };
    }

    use std::f64;
    const UIC_NORMALIZATION_CONSTANT: f64 = 1.5185673663648485;

    fn uic_proper(n:usize) -> f64
    {
        let mut res: f64 = 0.0;
        let mut n = n as f64;
        n = n.log(2.0);
        while n > 0.0{
            res = res + n;
            n = n.log(2.0);
        }

        res + UIC_NORMALIZATION_CONSTANT
    }

    #[inline]
    pub fn uic_memoized(n: usize) -> f64 {
        if n < MAX_UIC_MEMOIZATION
        {
            let mut res: f64 = UIC[n-1].get();
            if res != 0.0
            {
                res
            }
            else
            {
                res = uic_proper(n);
                UIC[n-1].set(res);
                res
            }
        }
        else
        {
            uic_proper(n)
        }
    }
}
