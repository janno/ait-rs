
use std::sync::atomic::{AtomicU64 as StdAtomicU64, Ordering};
use std::mem::transmute;

#[derive(Debug)]
pub struct AtomicF64 {
    inner: StdAtomicU64,
}

impl AtomicF64 {
    pub fn new(val: f64) -> AtomicF64 {
        AtomicF64 { inner: StdAtomicU64::new(f64_to_u64(val)) }
    }

    #[inline]
    pub fn get(&self) -> f64 {
        u64_to_f64(self.inner.load(Ordering::Relaxed))
    }

    #[inline]
    pub fn set(&self, val: f64) {
        self.inner.store(f64_to_u64(val), Ordering::Relaxed)
    }
}

fn u64_to_f64(val: u64) -> f64 {
    unsafe { transmute(val) }
}

fn f64_to_u64(val: f64) -> u64 {
    unsafe { transmute(val) }
}

